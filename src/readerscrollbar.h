/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2007 Vadim Lopatin <coolreader.org@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#ifndef READERSCROLLBAR_H
#define READERSCROLLBAR_H

#include <wx/scrolbar.h>

class ReaderView;

class ReaderScrollBar: public wxScrollBar
{
private:
    ReaderView* _view;
public:
    ReaderScrollBar(ReaderView* view) : _view(view) { }
    void OnSetFocus(wxFocusEvent& event);
private:
    DECLARE_EVENT_TABLE()
};

#endif // READERSCROLLBAR_H
