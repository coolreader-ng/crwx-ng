
# Find includes in corresponding current build & source directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(SRC_LIST
  readerapp.cpp
  readerframe.cpp
  readerscrollbar.cpp
  readerview.cpp
  rescont.cpp
  crwxscreen.cpp
  utils.cpp
  histlist.cpp
  optdlg.cpp
  toc.cpp
  wolopt.cpp
  aboutdialog.cpp
)

if(WIN32)
  set(SRC_LIST ${SRC_LIST} crwx.rc)
  set_property(SOURCE crwx.rc PROPERTY OBJECT_DEPENDS ${CMAKE_BINARY_DIR}/config.h)
endif(WIN32)

link_directories(${wxWidgets_LIBRARY_DIRS})
add_executable(crwx ${SRC_LIST})
target_link_libraries(crwx crengine-ng::crengine-ng ${wxWidgets_LIBRARIES})

if(UNIX)
  install(TARGETS crwx RUNTIME DESTINATION bin)
  install(DIRECTORY ../data/textures DESTINATION share/crwx)
  install(DIRECTORY ../data/backgrounds DESTINATION share/crwx)
  install(FILES desktop/crwx.desktop DESTINATION share/applications)
  #install(FILES desktop/crwx.appdata.xml DESTINATION share/metainfo)
  install(FILES desktop/crwx.png DESTINATION share/pixmaps)
  install(FILES desktop/crwx.xpm DESTINATION share/pixmaps)
  install(FILES desktop/crwx.png DESTINATION share/icons/hicolor/48x48/apps)
  install(FILES desktop/crwx.svg DESTINATION share/icons/hicolor/scalable/apps)
else()
  install(TARGETS crwx RUNTIME DESTINATION .)
  install( DIRECTORY ../data/textures DESTINATION .)
  install( DIRECTORY ../data/backgrounds DESTINATION .)
endif()
