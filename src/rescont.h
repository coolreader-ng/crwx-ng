/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2007 Vadim Lopatin <coolreader.org@gmail.com>           *
 *   Copyright (C) 2020,2023 Aleksey Chernov <valexlin@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#ifndef RESCONT_H
#define RESCONT_H

#include <wx/image.h>
#include <wx/bitmap.h>

#include <lvtypes.h>
#include <lvcontainer.h>

class ResourceContainer
{
private:
    LVContainerRef _container;
    ResourceContainer() { }
public:
    static ResourceContainer& instance();
    bool OpenFromStream(LVStreamRef stream);
    bool OpenFromMemory(const void* buf, int size);
    bool OpenFromFile(const lChar32* fname);
    bool OpenFromFile(const lChar8* fname);
    wxImage GetImage(const lChar32* pathname);
    wxBitmap GetBitmap(const lChar32* pathname);
};

#endif // RESCONT_H
