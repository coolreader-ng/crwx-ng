/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2007,2009,2012 Vadim Lopatin <coolreader.org@gmail.com> *
 *   Copyright (C) 2020,2023 Aleksey Chernov <valexlin@gmail.com>          *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#include "histlist.h"
#include "utils.h"

HistList::HistList() : wxListView(), _records(NULL) { }

HistList::~HistList() {
    if (_records)
        delete _records;
}

void HistList::SetRecords(LVPtrVector<CRFileHistRecord>& records) {
    if (_records)
        delete _records;
    _records = new LVPtrVector<CRFileHistRecord>(records);
    SetItemCount(_records->length());
    UpdateColumns();
    if (GetItemCount() > 0)
        Select(0);
}

void HistList::UpdateColumns() {
    // Using the wxLC_VIRTUAL flag wxLIST_AUTOSIZE does not work,
    //  so here we need to calculate the maximum column width.
    // With wxGTK-3.0 for some reason wxWindow::GetTextExtent() returns too large width
    // We can use wxClientDC::GetTextExtent() or wxGTK-3.2.
    //wxClientDC dc(this);
    wxSize em = GetTextExtent("M");
    LVArray<int> maxTextWidth(3, (int)wxLIST_AUTOSIZE_USEHEADER);
    for (int row = 0; row < GetItemCount(); row++) {
        for (int col = 0; col < 3; col++) {
            wxString text = GetItemText(row, col);
            int textWidth = GetTextExtent(text).x + em.x;
            //int textWidth = dc.GetTextExtent(text).x + 16;
            if (maxTextWidth[col] < textWidth)
                maxTextWidth[col] = textWidth;
        }
    }
    SetColumnWidth(0, maxTextWidth[0]);
    SetColumnWidth(1, maxTextWidth[1]);
    SetColumnWidth(2, maxTextWidth[2]);
}

bool HistList::Create(wxWindow* parent, wxWindowID id) {
    bool res = wxListView::Create(parent, id, wxDefaultPosition, wxDefaultSize,
                                  wxLC_REPORT | wxLC_VIRTUAL | wxLC_SINGLE_SEL);
    wxListItem col;
    col.SetText(wxString(wxT("Last time")));
    col.SetAlign(wxLIST_FORMAT_CENTRE);
    InsertColumn(0, col);

    col.SetAlign(wxLIST_FORMAT_LEFT);
    col.SetText(wxString(wxT("Book")));
    InsertColumn(1, col);

    col.SetText(wxString(wxT("Pos")));
    InsertColumn(2, col);

    SetItemCount(20);
    UpdateColumns();
    Update();
    return res;
}

wxString HistList::OnGetItemText(long item, long column) const {
    if (_records && item >= 0 && item < _records->length()) {
        CRFileHistRecord* rec = (*_records)[item];
        lString32 data;
        switch (column) {
            case 0:
                data = rec->getLastTimeString();
                break;
            case 1: {
                lString32 fname = rec->getFileName();
                lString32 author = rec->getAuthor();
                lString32 title = rec->getTitle();
                lString32 series = rec->getSeries();
                if (!series.empty()) {
                    if (!title.empty())
                        title << " ";
                    title << series;
                }
                if (!author.empty() && !title.empty())
                    author << ". ";
                data << author << title;
                if (data.empty())
                    data = fname;
            } break;
            case 2: {
                data = lString32::itoa(rec->getLastPos()->getPercent() / 100) + "%";
            } break;
        }
        return cr2wx(data);
    } else
        return wxString(wxT(""));
}
