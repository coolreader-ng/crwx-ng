/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2024 Aleksey Chernov <valexlin@gmail.com>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#ifndef APP_PROPS_H
#define APP_PROPS_H

#define PROP_APP_WINDOW_RECT             "window.rect"
#define PROP_APP_WINDOW_FULLSCREEN       "window.fullscreen"
#define PROP_APP_WINDOW_MINIMIZED        "window.minimized"
#define PROP_APP_WINDOW_MAXIMIZED        "window.maximized"
#define PROP_APP_WINDOW_SHOW_MENU        "window.menu.show"
#define PROP_APP_WINDOW_TOOLBAR_SIZE     "window.toolbar.size"
#define PROP_APP_WINDOW_TOOLBAR_POSITION "window.toolbar.position"
#define PROP_APP_WINDOW_SHOW_STATUSBAR   "window.statusbar.show"
#define PROP_APP_OPEN_LAST_BOOK          "app.init.open-recent"
#define PROP_APP_PAGE_VIEW_MODE          "app.page.view.mode"

#endif // APP_PROPS_H
