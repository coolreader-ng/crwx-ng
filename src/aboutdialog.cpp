/***************************************************************************
 *   crwx-ng                                                               *
 *   Copyright (C) 2024 Aleksey Chernov <valexlin@gmail.com>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License           *
 *   as published by the Free Software Foundation; either version 2        *
 *   of the License, or (at your option) any later version.                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the Free Software           *
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,            *
 *   MA 02110-1301, USA.                                                   *
 ***************************************************************************/

#include "config.h"
#include "aboutdialog.h"
#include <crengine-ng-config.h>

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/html/htmlwin.h>
#include <wx/utils.h>
#include <wx/settings.h>

BEGIN_EVENT_TABLE(AboutDialog, wxDialog)
    EVT_INIT_DIALOG(AboutDialog::OnInitDialogData)
    EVT_HTML_LINK_CLICKED(Window_Id_Html, AboutDialog::OnLinkClicked)
END_EVENT_TABLE()

AboutDialog::AboutDialog(wxWindow* parent)
        : wxDialog(parent, wxID_ANY, wxString(L"About"), wxDefaultPosition, wxDefaultSize,
                   wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER) {
    wxSize emSize = parent->GetTextExtent("M");

    wxString project_src_url = wxT("https://gitlab.com/coolreader-ng/crwx-ng/");

    wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer* titleSizer = new wxBoxSizer(wxHORIZONTAL);

    wxStaticText* nameText = new wxStaticText(this, wxID_ANY, wxT("CoolReaderNG/wx " VERSION));
    wxFont font = nameText->GetFont();
    font.SetPointSize(16 * font.GetPointSize() / 10);
    nameText->SetFont(font);
    titleSizer->Add(nameText, 0, wxALIGN_CENTER_VERTICAL | wxLEFT, emSize.x / 2);

    titleSizer->Add(2 * emSize.x, 1, 10, wxEXPAND, 0);

    wxStaticText* creVerText = new wxStaticText(this, wxID_ANY, wxT("crengine-ng " CRE_NG_VERSION));
    titleSizer->Add(creVerText, 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, emSize.x / 2);

    sizer->Add(titleSizer, 0, wxEXPAND, 0);

    wxHtmlWindow* htmlWindow = new wxHtmlWindow(this, Window_Id_Html);
    htmlWindow->SetMinSize(wxSize(44 * emSize.x, 18 * emSize.y));

    wxString aboutText = wxT("<html><body><p>");
    aboutText += wxT("CoolReaderNG/wx is free open source e-book viewer based on crengine-ng library.");
    aboutText += wxT("<br/>");
    aboutText += wxString("Source code is available at <a href=\"") + project_src_url + wxString("\">") +
                 project_src_url + wxString("</a> ");
    aboutText += wxT("under the terms of GNU GPL license either version 2 or (at your option) any later version.");
    aboutText += wxT("<br/>");
    aboutText += wxT("It is a fork of the 'CoolReader' program.");
    aboutText += wxT("</p>");

    aboutText += wxT("<p><u>");
    aboutText += wxT("Third party components used in crengine-ng:");
    aboutText += wxT("</u></p><ul>");

#if USE_FREETYPE == 1
    aboutText += wxT("<li>");
    aboutText += wxT("FreeType - font rendering");
    aboutText += wxT("</li>");
#endif
#if USE_HARFBUZZ == 1
    aboutText += wxT("<li>");
    aboutText += wxT("HarfBuzz - text shaping, font kerning, ligatures");
    aboutText += wxT("</li>");
#endif
#if USE_ZLIB == 1
    aboutText += wxT("<li>");
    aboutText += wxT("ZLib - compressing library (zip archive support, cache compression)");
    aboutText += wxT("</li>");
#endif
#if USE_ZSTD == 1
    aboutText += wxT("<li>");
    aboutText += wxT("ZSTD - compressing library (cache compression)");
    aboutText += wxT("</li>");
#endif
#if USE_LIBPNG == 1
    aboutText += wxT("<li>");
    aboutText += wxT("libpng - PNG image format support");
    aboutText += wxT("</li>");
#endif
#if USE_LIBJPEG == 1
    aboutText += wxT("<li>");
    aboutText += wxT("libjpeg - JPEG image format support");
    aboutText += wxT("</li>");
#endif
#if USE_FRIBIDI == 1
    aboutText += wxT("<li>");
    aboutText += wxT("FriBiDi - RTL writing support");
    aboutText += wxT("</li>");
#endif
#if USE_LIBUNIBREAK == 1
    aboutText += wxT("<li>");
    aboutText += wxT("libunibreak - line breaking and word breaking algorithms");
    aboutText += wxT("</li>");
#endif
#if USE_UTF8PROC == 1
    aboutText += wxT("<li>");
    aboutText += wxT("utf8proc - for unicode string comparision");
    aboutText += wxT("</li>");
#endif
#if USE_NANOSVG == 1
    aboutText += wxT("<li>");
    aboutText += wxT("NanoSVG - SVG image format support");
    aboutText += wxT("</li>");
#endif
#if USE_CHM == 1
    aboutText += wxT("<li>");
    aboutText += wxT("chmlib - chm format support");
    aboutText += wxT("</li>");
#endif
#if USE_ANTIWORD == 1
    aboutText += wxT("<li>");
    aboutText += wxT("antiword - Microsoft Word format support");
    aboutText += wxT("</li>");
#endif
#if USE_SHASUM == 1
    aboutText += wxT("<li>");
    aboutText += wxT("RFC6234 (sources) - SHAsum");
    aboutText += wxT("</li>");
#endif
#if USE_CMARK_GFM == 1
    aboutText += wxT("<li>");
    aboutText += wxT("cmark-gfm - GitHub's fork of cmark, a CommonMark parsing and rendering library and program in C");
    aboutText += wxT("</li>");
#endif
#if USE_MD4C == 1
    aboutText += wxT("<li>");
    aboutText += wxT("MD4C - MD4C stands for \"Markdown for C\" and that's exactly what this project is about");
    aboutText += wxT("</li>");
#endif
    aboutText += wxT("<li>");
    aboutText += wxT("hyphman - AlReader hyphenation manager");
    aboutText += wxT("</li>");
    aboutText += wxT("<li>");
    aboutText += wxT("Most hyphenation dictionaries - TEX hyphenation patterns");
    aboutText += wxT("</li>");
    aboutText += wxT("<li>");
    aboutText += wxT("Russian hyphenation dictionary - https://github.com/laboratory50/russian-spellpack");
    aboutText += wxT("</li>");
#if USE_LOCALE_DATA
    aboutText += wxT("<li>");
    aboutText += wxT("Languages character set database by Fontconfig");
    aboutText += wxT("</li>");
#endif
    aboutText += wxT("</ul>");
    aboutText += wxT("</body></html>");
    htmlWindow->SetPage(aboutText);
    sizer->Add(htmlWindow, 10, wxEXPAND | wxALL, emSize.x);

    wxSizer* btnSizer = CreateButtonSizer(wxOK);
    sizer->Add(btnSizer, 0, wxALIGN_CENTER | wxALL, emSize.x);

    sizer->SetSizeHints(this);
    SetSizer(sizer);
}

AboutDialog::~AboutDialog() { }

void AboutDialog::OnInitDialogData(wxInitDialogEvent& event) { }

void AboutDialog::OnLinkClicked(wxHtmlLinkEvent& event) {
    wxLaunchDefaultBrowser(event.GetLinkInfo().GetHref(), wxBROWSER_NEW_WINDOW);
}
